package ai.maum.automl.frontapi.model.dto;

import ai.maum.automl.frontapi.commons.utils.ModelMapperService;
import ai.maum.automl.frontapi.model.entity.project.Project;
import ai.maum.automl.frontapi.model.entity.project.ProjectStt;
import ai.maum.automl.frontapi.model.entity.project.LearningData;
import ai.maum.automl.frontapi.repository.ProjectRepository;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.annotations.DynamicUpdate;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by kirk@mindslab.ai on 2020-10-20
 */
@SpringBootTest
@DynamicUpdate
@Slf4j
class ProjectSttDtoTest {
    @Autowired
    ProjectRepository projectRepo;

//    @Autowired
//    ModelSttRepository modelRepo;

    @Autowired
    ModelMapperService modelMapperService;
//
//    @Test
//    @Transactional
//    @Rollback(value = false)
//    public void newProject() {
//
//        ProjectDto pd = new ProjectDto();
//        pd.setName("DTO TEST2");
//        pd.setDescription("DTO CREATE TEST2");
//        pd.setProjectType("STT");
//
//        Project project = modelMapperService.map(pd, Project.class);
//        User user = new User(2L);
//        project.setUser(user);
//        ProjectStt ms = new ProjectStt();
//        ms.setCompletedStep(0);
//        project.setModelStt(ms);
//        projectRepo.save(project);
//    }
//
    @Test
    @Transactional
    @Rollback(value = true)
    public void updateModelDataSetting() {
        Optional<Project> optional = projectRepo.findById(1L);

        if(optional.isPresent()) {
            ProjectStt ps = (ProjectStt)optional.get();
            ps.setLanguage("KOKOKO1111111");
            ps.setSampleRate("999992222");
            ps.setEos('N');

            List<LearningData> learningDataList = new ArrayList<>();
            for(int i=0; i<5 ;i++) {
                LearningData d = new LearningData();
                d.setSmplRate("SR001");
                d.setProject(ps);
                learningDataList.add(d);
            }
            ps.getLearningDataList().clear();
            ps.getLearningDataList().addAll(learningDataList);
        }

    }

}