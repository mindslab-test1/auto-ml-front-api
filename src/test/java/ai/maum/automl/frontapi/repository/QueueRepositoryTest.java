package ai.maum.automl.frontapi.repository;

import ai.maum.automl.frontapi.model.entity.project.GraphDataSnapshot;
import ai.maum.automl.frontapi.model.entity.project.ProjectSnapshot;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.annotations.DynamicUpdate;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@SpringBootTest
@DynamicUpdate
@Slf4j
class QueueRepositoryTest {
    @Autowired
    QueueRepository repo;

    @Autowired
    ProjectSnapshotRepository projectSnapshotRepository;

    @Autowired
    GraphDataRepository graphDataRepository;

    @Autowired
    GraphDataSnapshotRepository graphDataSnapshotRepository;

    @Test
    @Rollback(value = true)
    public void QueueCountTest() {
        int cnt = repo.findOrder(5L);
        System.out.println("Count = " + cnt);
    }

    @Test
    @Rollback(value = true)
    public void projectSnapshotRepoTest() {
        Optional<ProjectSnapshot> result = projectSnapshotRepository.findTop1ByProjectIdOrderByRegDtDesc(1L);
        if(result.isPresent()) {
            System.out.println(result);
        } else {
            System.out.println("EMPTY!!!!");
        }
    }


    @Test
    @Rollback(value = true)
    public void proejctSnapshotTest() {
        List<GraphDataSnapshot.DataPoint> graphDataSnapshotList = graphDataSnapshotRepository.findByProjectSnapshotIdOrderByRegDtAsc(76L);
        if(graphDataSnapshotList != null) {
            System.out.println(graphDataSnapshotList);

            Gson gson = new Gson();
            Type listType = new TypeToken<ArrayList<GraphDataSnapshot>>(){}.getType();
            List<GraphDataSnapshot> result = gson.fromJson(graphDataSnapshotList.toString(), listType);

            System.out.println("@@@@@@@@@@@@@@@");
            System.out.println(result.toString());

        } else {
            System.out.println("EMPTY!!!!");
        }
    }

/*    @Test
    @Rollback(value = true)
    public void projectSnapshotList() {
        String graphDataSnapshotList =  graphDataRepository.findByProjectSnapshotId(71L).getDataPoints();
        if(graphDataSnapshotList != null ) {
            System.out.println(graphDataSnapshotList);
        } else {
            System.out.println("EMPTY!!!!");
        }
    }*/

}