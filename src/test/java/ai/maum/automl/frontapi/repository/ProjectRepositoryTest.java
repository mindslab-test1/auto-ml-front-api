package ai.maum.automl.frontapi.repository;

import ai.maum.automl.frontapi.commons.utils.ModelMapperService;
import ai.maum.automl.frontapi.model.entity.common.User;
import ai.maum.automl.frontapi.model.entity.project.Project;
import ai.maum.automl.frontapi.model.entity.project.ProjectStt;
import ai.maum.automl.frontapi.model.entity.project.ProjectTts;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.annotations.DynamicUpdate;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;

import javax.transaction.Transactional;
import java.util.Optional;

/**
 * Created by kirk@mindslab.ai on 2020-10-19
 */

@SpringBootTest
@DynamicUpdate
@Slf4j
class ProjectRepositoryTest {
    @Autowired
    ProjectRepository repo;

    @Autowired
    ModelMapperService modelMapperService;

    @Test
    @Rollback(value = true)
    public void newProject() {

//        User user = new User(2L);
//        ProjectStt project = new ProjectStt();
//        project.setProjectName("stt project 1");
//        project.setProjectDescription("stt project 1 desc");
//        project.setProjectType("STT");
//        project.setModelName("stt project 1 model");
//        project.setUser(user);
//
//        repo.save(project);
//
//        ProjectTts pt = new ProjectTts();
//        pt.setUser(user);
//        pt.setProjectName("tts project 1");
//        pt.setProjectDescription("tts project 1 desc");
//        pt.setProjectType("TTS");
//        pt.setModelName("tts project 1 model");
//        repo.save(pt);
    }

    @Test
    @Transactional
    @Rollback(value = false)
    public void get1() {

//        Optional<Project> o = repo.findById(1L);
//
//        if(o.isPresent()) {
//            Project p = o.get();
//            System.out.println(p.getProjectName());
//            ProjectStt ps = (ProjectStt)p;
//            System.out.println(ps.getModelName());
//        }

    }

}