package ai.maum.automl.frontapi.controller;

import ai.maum.automl.frontapi.commons.security.CurrentUser;
import ai.maum.automl.frontapi.commons.security.UserPrincipal;
import ai.maum.automl.frontapi.service.ModelTestService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping("/api/test")
public class ModelTestController {
    private final ModelTestService modelTestService;

    @ApiOperation(value = "모델 테스트 생성 요청")
    @PostMapping("/begin")
//    @ApiImplicitParams({
//            @ApiImplicitParam(name = "modelId", value = "모델 ID", dataType = "String"),
//            @ApiImplicitParam(name = "modelFileName", value = "모델 파일 이름", dataType = "String"),
//            @ApiImplicitParam(name = "modelFilePath", value = "모델 파일 경로", dataType = "String"),
//            @ApiImplicitParam(name = "dataFile", value = "데이터 파일", dataType = "MultipartFile")
//    })
    @PreAuthorize("hasAnyRole('USER')")
    public ResponseEntity<HashMap<String, Object>> beginModelTest(String modelId, String modelFileName, String modelFilePath, MultipartFile dataFile) {
        String logTitle = "beginModelTest/modelId[" + modelId + "] modelFileName[" + modelFileName + "] modelFilePath[" + modelFilePath + "] dataFile[" + dataFile + "]/";

        try {
            HashMap<String, Object> responseBody = modelTestService.beginModelTest(modelId, modelFileName, modelFilePath, dataFile);
            log.info(logTitle + "success");
            return new ResponseEntity<>(responseBody, HttpStatus.OK);
        } catch (Exception e) {
            log.error(logTitle + "failed : " + e.getMessage());
            e.printStackTrace();
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ApiOperation(value = "모델 테스트의 상태값 조회")
    @GetMapping("/status")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "modelTestSnapshotId", value = "스냅샷 ID", dataType = "Long")
    })
    @PreAuthorize("hasAnyRole('USER')")
    public ResponseEntity<HashMap<String, Object>> getModelTestStatus(@CurrentUser UserPrincipal user,
                                                                      Long modelTestSnapshotId) {
        Long userNo = user.getUserNo();
        String logTitle = "getModelTestStatus/user[" + userNo + "] modelTestSnapshotId[" + modelTestSnapshotId + "]/";
        
        try {
            HashMap<String, Object> responseBody = modelTestService.getModelTestStatus(userNo, modelTestSnapshotId);
            log.info(logTitle + "success");
            return new ResponseEntity<>(responseBody, HttpStatus.OK);
        } catch (Exception e) {
            log.error(logTitle + "failed : " + e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ApiOperation(value = "대기열에 있거나 실행 중인 모델 테스트 취소")
    @GetMapping("/cancel")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "modelTestSnapshotId", value = "스냅샷 ID", dataType = "Long")
    })
    @PreAuthorize("hasAnyRole('USER')")
    public ResponseEntity<HashMap<String, Object>> cancelModelTest(@CurrentUser UserPrincipal user,
                                                                   Long modelTestSnapshotId) {
        Long userNo = user.getUserNo();
        String logTitle = "cancelModelTest/user[" + userNo + "] modelTestSnapshotId[" + modelTestSnapshotId + "]/";

        try {
            HashMap<String, Object> responseBody = modelTestService.cancelModelTest(userNo, modelTestSnapshotId);
            log.info(logTitle + "success");
            return new ResponseEntity<>(responseBody, HttpStatus.OK);
        } catch (Exception e) {
            log.error(logTitle + "failed : " + e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
