package ai.maum.automl.frontapi.controller;

import ai.maum.automl.frontapi.commons.security.CurrentUser;
import ai.maum.automl.frontapi.commons.security.UserPrincipal;
import ai.maum.automl.frontapi.model.dto.project.ProjectDto;
import ai.maum.automl.frontapi.service.SttProjectService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

/**
 * Created by kirk@mindslab.ai on 2020-10-20
 */
@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping("/api/project/stt")
public class SttProjectController {

    private final SttProjectService sttProjectService;


    @ApiOperation(value = "STT 프로젝트 생성")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "dto", value = "프로젝트 기본정보", dataType = "ProjectDto")
    })
    @PostMapping
    @PreAuthorize("hasAnyRole('USER')")
    public ResponseEntity<ProjectDto.Stt> add(@CurrentUser UserPrincipal user,
                                 @RequestBody ProjectDto dto) {
        String logTitle = "add/" + dto.toString() + "/";
        ProjectDto.Stt project = sttProjectService.add(user.getUserNo(), dto);

        if(project != null) {
            log.info(logTitle + "project id:" + project.getId());
            return new ResponseEntity<>(project, HttpStatus.CREATED);
        }
        else {
            log.error(logTitle + "failed");
            return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
        }
    }


    @ApiOperation(value = "STT 프로젝트 조회")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "프로젝트 ID", dataType = "Long")
    })
    @GetMapping("/{id}")
    @PreAuthorize("hasAnyRole('USER')")
    public ResponseEntity<ProjectDto.Stt> get(@CurrentUser UserPrincipal user,
                                              @PathVariable Long id) {
        String logTitle = "get/id[" + id + "]/";

        try {
            ProjectDto.Stt dto = sttProjectService.get(user.getUserNo(), id);
            log.info(logTitle + dto.toString());
            return new ResponseEntity<>(dto, HttpStatus.OK);
        } catch (EmptyResultDataAccessException e) {
            log.info(logTitle + "failed.");
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }


    @ApiOperation(value = "STT 프로젝트 step update")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "프로젝트 ID", dataType = "Long"),
            @ApiImplicitParam(name = "step", value = "바꿀 STEP", dataType = "int")
    })
    @PostMapping("/{id}/{step}")
    @PreAuthorize("hasAnyRole('USER')")
    public ResponseEntity<?> updateStep(@CurrentUser UserPrincipal user,
                                             @PathVariable Long id,
                                             @PathVariable int step){
        String logTitle = "update step/id[" + id + "], step[" + step + "]/";
        try{
            sttProjectService.updateStep(user.getUserNo(), id, step);
        }catch(EmptyResultDataAccessException e){
            log.error(logTitle + "failed.");
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        log.info(logTitle + "done.");
        return new ResponseEntity<>(HttpStatus.OK);
    }



    @ApiOperation(value = "STT 프로젝트 학습 데이터 정보 입력")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "프로젝트 ID", dataType = "Long")
    })
    @PostMapping("/{id}/learningData")
    @PreAuthorize("hasAnyRole('USER')")
    public ResponseEntity<?> learningData(@CurrentUser UserPrincipal user,
                                        @PathVariable Long id,
                                        @RequestBody ProjectDto.LearningDataInput dto) {
        String logTitle = "update learningData/id[" + id + "], " + dto.toString() + "/";
        try {
            sttProjectService.learningData(user.getUserNo(), id, dto);
        } catch (EmptyResultDataAccessException e) {
            log.error(logTitle + "failed.");
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        log.info(logTitle + "done.");
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @ApiOperation(value = "STT 프로젝트 학습 설정 정보 입력")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "프로젝트 ID", dataType = "Long")
    })
    @PostMapping("/{id}/trainingData")
    @PreAuthorize("hasAnyRole('USER')")
    public ResponseEntity<?> trainingData(@CurrentUser UserPrincipal user,
                                            @PathVariable Long id,
                                            @RequestBody ProjectDto.TrainingDataInput dto) {
        String logTitle = "update trainingData/id[" + id + "], " + dto.toString() + "/";
        try {
            sttProjectService.trainingData(user.getUserNo(), id, dto);
        } catch (EmptyResultDataAccessException e) {
            log.error(logTitle + "failed.");
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        log.info(logTitle + "done.");
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
