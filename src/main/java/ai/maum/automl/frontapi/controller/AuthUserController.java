package ai.maum.automl.frontapi.controller;

import ai.maum.automl.frontapi.commons.exception.StackTrace;
import ai.maum.automl.frontapi.commons.payload.ApiResponse;
import ai.maum.automl.frontapi.commons.security.CurrentUser;
import ai.maum.automl.frontapi.commons.security.UserPrincipal;
import ai.maum.automl.frontapi.model.dto.common.JwtTokenDto;
import ai.maum.automl.frontapi.model.dto.common.LoginDto;
import ai.maum.automl.frontapi.model.dto.project.UserSignupDto;
import ai.maum.automl.frontapi.model.entity.common.Role;
import ai.maum.automl.frontapi.model.entity.common.RoleName;
import ai.maum.automl.frontapi.model.entity.common.User;
import ai.maum.automl.frontapi.service.AuthService;
import ai.maum.automl.frontapi.service.UserService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping("/api/auth/user")
public class AuthUserController {


    private final AuthService authSvc;

    private final UserService userSvc;

    private final ModelMapper modelMapper = new ModelMapper();

    @Value("${sso.callback}")
    String OAUTH_CALLBACK_URI;

//    @PostMapping("/signup")
//    public ResponseEntity<ApiResponse> userSignup(@RequestBody @Validated UserSignupDto dto)
//    {
//        String logTitle = "userSignup/";
//
//        try {
//            if(userSvc.doesUserExist(dto.getEmail())) {
//                log.info(logTitle + "Email[{}], [Already exists]", dto.getEmail());
//                return new ResponseEntity<>(new ApiResponse(true, "이미 가입"), HttpStatus.OK);
//            }
//
//            Role userRole = userSvc.getRole(RoleName.ROLE_USER);
//            User user = modelMapper.map(dto, User.class);
//
//            user = userSvc.signup(user, userRole);
//
//            log.info(logTitle + "Email[{}], UserNo[{}], [Success]", user.getEmail(), user.getId());
//            return new ResponseEntity<>(new ApiResponse(true, "가입 성공"), HttpStatus.CREATED);
//        }
//        catch (Exception e) {
//            log.warn(logTitle + "Exception=" + StackTrace.getStackTrace(e));
//            return new ResponseEntity<>(new ApiResponse(false, "가입 실패"), HttpStatus.BAD_REQUEST);
//        }
//    }


    @ApiOperation(value = "로그인")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "dto", value = "로그인 정보", dataType = "LoginDto")
    })
    @PostMapping("/login")
    public ResponseEntity<?> login(@RequestBody LoginDto dto)
    {
        String logTitle = "login/Id[" + dto.getId() + "], Password[" + dto.getPassword() + "], ";

        Authentication auth;
        String jwt;
        try {
            auth = authSvc.authenticate(dto.getId(), dto.getPassword());
            jwt = authSvc.generateJwtToken(auth);
        }
        catch (Exception e) {
            log.warn(logTitle + e.getMessage());
            return new ResponseEntity<>(new ApiResponse(false, "로그인에 실패하였습니다."), HttpStatus.BAD_REQUEST);
        }

        log.info(logTitle + "[Success]", dto.getId());
        return ResponseEntity.ok(new JwtTokenDto(jwt, auth.getAuthorities()));
    }

    @GetMapping(value = "/callback")
    public ResponseEntity<?> authCallback(HttpServletRequest request,
                               HttpServletResponse response,
                               @RequestParam("code") String code,
                               @RequestParam("state") String state) throws Exception {

        String logTitle = "authCallback/code[" + code + "], state[" + state + "], ";

        Authentication auth;
        String jwt;

        User user = authSvc.getAuthToken(code, state, OAUTH_CALLBACK_URI);

        if(user != null) {
            // JWT 생성
            try {
                auth = authSvc.authenticate(user.getEmail(), user.getPassword());
                jwt = authSvc.generateJwtToken(auth);

                log.info(logTitle + "[Success]", user.toString());
                return ResponseEntity.ok(new JwtTokenDto(jwt, auth.getAuthorities()));
            } catch (Exception e) {
                log.warn(logTitle + e.getMessage());
                return new ResponseEntity<>(new ApiResponse(false, "로그인에 실패하였습니다."), HttpStatus.BAD_REQUEST);
            }
        }
        else {
            log.warn(logTitle + "[Fail]");
            return new ResponseEntity<>(new ApiResponse(false, "로그인에 실패하였습니다."), HttpStatus.BAD_REQUEST);
        }
    }


    @ApiOperation(value = "로그아웃")
    @PostMapping("/logout")
    @PreAuthorize("hasAnyRole('STORE_OWNER', 'STORE_EMPLOYEE', 'USER', 'ADMIN')")
    public ResponseEntity<?> logout(@CurrentUser UserPrincipal user)
    {
        String logTitle = "logout/ID[" + user.getUserNo() + "], ";

        try {
            log.info(logTitle + "[Logout]");
            return new ResponseEntity<>(HttpStatus.OK);
        }
        catch (Exception e) {
            log.warn(logTitle + StackTrace.getStackTrace(e));
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }


    @ApiOperation(value = "로그인 체크")
    @GetMapping("/ping")
    @PreAuthorize("hasAnyRole('STORE_OWNER', 'STORE_EMPLOYEE', 'USER', 'ADMIN')")
    public ResponseEntity<?> ping(@CurrentUser UserPrincipal user)
    {
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
