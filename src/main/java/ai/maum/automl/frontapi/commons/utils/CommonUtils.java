package ai.maum.automl.frontapi.commons.utils;

public class CommonUtils {

    public static String getImageExtFromBase64(String base64)
    {
        String[] strings = base64.split(",");
        if ("data:image/png;base64".equals(strings[0])) {
            return "png";
        }
        else if ("data:image/jpeg;base64".equals(strings[0])) {
            return "jpeg";
        }
        return "jpg";
    }
}
