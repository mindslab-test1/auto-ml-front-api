package ai.maum.automl.frontapi.commons.utils;

import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ModelMapperService {

    private final ModelMapper modelMapper;

    ModelMapperService()
    {
        this.modelMapper = new ModelMapper();
    }

    public <D, T> D map(final T entity, Class<D> outClass)
    {
        modelMapper.getConfiguration().setAmbiguityIgnored(false);
        return modelMapper.map(entity, outClass);
    }

    public <D, T> D mapAmbiguityIgnored(final T entity, Class<D> outClass)
    {
        modelMapper.getConfiguration().setAmbiguityIgnored(true);
        return modelMapper.map(entity, outClass);
    }

    public <D, T> List<D> mapAll(final Collection<T> entityList, Class<D> outClass)
    {
        return entityList.stream()
                .map(entity -> map(entity, outClass))
                .collect(Collectors.toList());
    }
}
