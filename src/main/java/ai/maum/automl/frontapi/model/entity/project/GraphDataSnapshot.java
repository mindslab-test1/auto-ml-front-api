package ai.maum.automl.frontapi.model.entity.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
@Table(name = "GRAPH_DATA_SNAPSHOT", indexes = {@Index(columnList = "projectSnapshotId")})
public class GraphDataSnapshot {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Lob
    private String dataPoints;

    private int epoch;

    private Long projectSnapshotId;

    @Column(nullable = false, updatable = false)
    @CreationTimestamp
    private LocalDateTime regDt;

    public static interface DataPoint {
        String getDataPoints();
    }
}

