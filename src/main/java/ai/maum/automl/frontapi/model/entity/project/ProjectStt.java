package ai.maum.automl.frontapi.model.entity.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
@DiscriminatorValue("STT")
public class ProjectStt extends Project {

    private int completedStep;

    // data setting
    private String language;
    private String sampleRate;
    private char eos;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "project", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<LearningData> learningDataList = null;

    public void setLearningDataList(List<LearningData> list) {
        if(this.learningDataList == null) {
            this.learningDataList = new ArrayList<>();
        }
        else {
            this.learningDataList.clear();
        }
        if(list == null) return;
        this.learningDataList.addAll(list);
        for (LearningData t: this.learningDataList) {
            if(t.getProject() != this)
                t.setProject(this);
        }
    }

    // training setting
    private int epochSize;
    private int batchSize;
    private int rate;
    private int sampleLength;

    private int minNoiseSample;
    private int maxNoiseSample;
    private int minSnr;
    private int maxSnr;
    private int checkSave;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "project", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<NoiseData> noiseDataList = null;

    public void setNoiseDataList(List<NoiseData> list) {
        if(this.noiseDataList == null) {
            this.noiseDataList = new ArrayList<>();
        }
        else {
            this.noiseDataList.clear();
        }
        if(list == null) return;
        this.noiseDataList.addAll(list);
        for(NoiseData t: this.noiseDataList) {
            if(t.getProject() != this)
                t.setProject(this);
        }
    }

    @OneToOne(fetch = FetchType.LAZY, mappedBy = "project", cascade = CascadeType.ALL, orphanRemoval = true)
    private PreTrainedModel preTrainedModel;
    public void setPreTrainedModel(PreTrainedModel model) {
        model.setProject(this);
        this.preTrainedModel = model;
    }
}
