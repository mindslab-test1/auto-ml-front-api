package ai.maum.automl.frontapi.model.entity.project;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * Created by kirk@mindslab.ai on 2020-10-19
 */
@Entity
@Getter
@Setter
@NoArgsConstructor
public class PreTrainedModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    private Long id;

    private String mdlId;
//    private String mdlGrp;
    private String mdlFileName;
    private String mdlFilePath;
//    private String mdlFileSize;
//    private String regDate;
//    private String language;
//    private String sampleRate;
//    private String useEosYn;

    @Column(nullable = false, updatable = false)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonIgnore
    @CreationTimestamp
    private LocalDateTime regDt;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "project_id")
    @JsonIgnore
    private Project project;
}
