package ai.maum.automl.frontapi.model.entity.project;

import ai.maum.automl.frontapi.model.entity.common.User;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@NoArgsConstructor
@Getter
@Setter
@Table(name = "model_inquiry")
public class ModelInquiry {

    @Column(name = "id")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "user_id", updatable = false)
    private User user;

    private String name;

    private String email;

    private String phone;

    private String modelName;

    @Column(columnDefinition = "mediumtext")
    private String inquiryDesc;

    @Column(nullable = false, updatable = false)
    @CreationTimestamp
    private LocalDateTime regDt;
}
