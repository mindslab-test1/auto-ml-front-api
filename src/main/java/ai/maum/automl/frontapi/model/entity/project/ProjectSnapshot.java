package ai.maum.automl.frontapi.model.entity.project;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * Created by kirk@mindslab.ai on 2020-10-21
 */
@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(indexes = {@Index(columnList = "projectId")})
public class ProjectSnapshot {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    @Lob
    private String snapshot;

    @Column(nullable = false, updatable = false)
    @CreationTimestamp
    private LocalDateTime regDt;

    private Long projectId;

    private String modelId;

    private String status;

    public static interface ModelId {
        String getModelId();
    }
}
