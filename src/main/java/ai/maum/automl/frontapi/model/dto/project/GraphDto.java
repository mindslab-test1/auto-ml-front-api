package ai.maum.automl.frontapi.model.dto.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class GraphDto {
    String status;
    int order;
    String modelId;
    Long snapshotId;
    Object dataPoints;
}
