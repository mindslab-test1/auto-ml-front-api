package ai.maum.automl.frontapi.model.dto.project;

import lombok.Getter;

@Getter
public class UserSignupDto {

    private String name;

    private String email;

    private String password;
}
