package ai.maum.automl.frontapi.model.entity.project;

import java.time.LocalDateTime;

/**
 * Created by kirk@mindslab.ai on 2020-11-24
 */
public interface ProjectExInterface {
    Long getId();
    String getProjectName();
    String getProjectDescription();
    String getProjectType();
    String getModelName();
    LocalDateTime getRegDt();
    LocalDateTime getModDt();
    Integer getModelCnt();
    String getStatus();
}
