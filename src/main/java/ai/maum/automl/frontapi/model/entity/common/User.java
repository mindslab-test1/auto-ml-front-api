package ai.maum.automl.frontapi.model.entity.common;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@NoArgsConstructor
@Getter
@Setter
@Table(name = "user", indexes = {@Index(columnList = "email")})
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private String email;

    private String apiId;

    private String apiKey;

    @Column(name="reg_dt", nullable = false, updatable = false)
    @CreationTimestamp
    private LocalDateTime regDt;

    @ManyToOne
    @JoinColumn(name="role_id", nullable = false, updatable = false)
    private Role role;

    private String password;

    public User (Long id) {
        this.id = id;
    }
}
