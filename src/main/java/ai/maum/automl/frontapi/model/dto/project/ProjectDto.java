package ai.maum.automl.frontapi.model.dto.project;

import ai.maum.automl.frontapi.model.entity.project.NoiseData;
import ai.maum.automl.frontapi.model.entity.project.PreTrainedModel;
import ai.maum.automl.frontapi.model.entity.project.LearningData;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@ToString
public class ProjectDto implements Serializable {

    private Long id;
    private String projectName;
    private String projectDescription;
    private String projectType;
    private String modelName;

    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss", timezone = "Asia/Seoul")
    private LocalDateTime regDt;

    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss", timezone = "Asia/Seoul")
    private LocalDateTime modDt;

    @Getter
    @Setter
    @NoArgsConstructor
    @ToString
    @EqualsAndHashCode(callSuper = true)
    @FieldDefaults(level = AccessLevel.PRIVATE)
    public static class Stt extends ProjectDto {
        private String modelName;
        private int completedStep;

        // data setting
        private String language;
        private String sampleRate;
        private char eos;
        private List<LearningData> learningDataList;

        // training setting
        private int epochSize;
        private int batchSize;
        private int rate;
        private int sampleLength;
        private int minNoiseSample;
        private int maxNoiseSample;
        private int minSnr;
        private int maxSnr;
        private int checkSave;
        private List<NoiseData> noiseDataList;
        private PreTrainedModel preTrainedModel;
    }

    /**
     * LearningData input dto
     */
    @Getter
    @Setter
    @NoArgsConstructor
    @ToString
    @FieldDefaults(level = AccessLevel.PRIVATE)
    public static class LearningDataInput {
        // data setting
        private String language;
        private String sampleRate;
        private char eos;
        private List<LearningData> learningDataList;
    }

    /**
     * TrainingData input dto
     */
    @Getter
    @Setter
    @NoArgsConstructor
    @ToString
    @FieldDefaults(level = AccessLevel.PRIVATE)
    public static class TrainingDataInput {
        // training setting
        private int epochSize;
        private int batchSize;
        private int rate;
        private int sampleLength;
        private int minNoiseSample;
        private int maxNoiseSample;
        private int minSnr;
        private int maxSnr;
        private int checkSave;
        private List<NoiseData> noiseDataList;
        private PreTrainedModel preTrainedModel;
    }

    @Getter
    @Setter
    @NoArgsConstructor
    @ToString
    @EqualsAndHashCode(callSuper = true)
    @FieldDefaults(level = AccessLevel.PRIVATE)
    public static class Tts extends ProjectDto {
        private String modelName;
        private int completedStep;
    }

    @Getter
    @Setter
    @NoArgsConstructor
    @ToString
    @EqualsAndHashCode(callSuper = true)
    @FieldDefaults(level = AccessLevel.PRIVATE)
    public static class ProjectDtoEx extends ProjectDto {
        private String status;
        private int modelCnt;
    }
}
