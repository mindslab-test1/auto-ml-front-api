package ai.maum.automl.frontapi.model.entity.common;

public enum RoleName {
    ROLE_ADMIN,
    ROLE_USER,
    ROLE_USER_EX1,
    ROLE_USER_EX2
}
