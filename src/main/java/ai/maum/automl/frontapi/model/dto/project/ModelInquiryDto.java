package ai.maum.automl.frontapi.model.dto.project;

import lombok.Getter;


@Getter
public class ModelInquiryDto {
    private Long userId;
    private String name;
    private String email;
    private String phoneNumber;
    private String inquiryDesc;
    private String modelName;
}
