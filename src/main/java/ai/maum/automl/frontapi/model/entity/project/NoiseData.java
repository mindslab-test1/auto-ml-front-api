package ai.maum.automl.frontapi.model.entity.project;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * Created by kirk@mindslab.ai on 2020-10-19
 */
@Entity
@Getter
@Setter
@NoArgsConstructor
public class NoiseData {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    private Long id;

    private String atchFileId;
    private String atchFileDisp;
    private String atchFilePath;
    private String atchFileName;
    private String orgFileName;
    private String atchFileType;
    private long   atchFileSize;
    private String smplRate;
    private float playSec;
    private float playSecAvg;
    private float minPlaySec;
    private float maxPlaySec;
    private int totFileCount;
    private String langCd;
    private String useEosYn;

    @Column(nullable = false, updatable = false)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @CreationTimestamp
    private LocalDateTime regDt;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "project_id")
    @JsonIgnore
    private Project project;
}
