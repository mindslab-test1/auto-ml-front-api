package ai.maum.automl.frontapi.model.entity.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * Created by kirk@mindslab.ai on 2020-10-20
 */
@Entity
@Getter
@Setter
@NoArgsConstructor
@DiscriminatorValue("TTS")
public class ProjectTts extends Project{
    private int completedStep;
}
