package ai.maum.automl.frontapi.legacy;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

/**
 * Created by yejoon3117@mindslab.ai on 2020-11-25
 */
@Getter
@Setter
@NoArgsConstructor
@ToString
public class ModelInfoVo implements Serializable {
    String mdlGrp;          // 모델 그룹명 (mdl_userID_엔진타입)
    String userId;          // 사용자 ID
    String engnType;        // 엔진 타입
    int totFileCount;       // 총 파일 개수
    String modelPath;       // 해당 모델 최상위 디렉토리
    String regDate;         // 모델 등록 일시
    String modDate;         // 모델에 마지막 파일이 추가된 일시
    String langCd;          // 언어
    String smplRate;        // Sampling Rate
    String useEosYn;        // EOS적용여부 (Y/N/null)
    List<ModelFileInfoVo> modelFileInfoVoList;  // 모델 데이터 파일 정보 리스트
}
