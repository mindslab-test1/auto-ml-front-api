package ai.maum.automl.frontapi.legacy;

import ai.maum.automl.frontapi.commons.security.CurrentUser;
import ai.maum.automl.frontapi.commons.security.UserPrincipal;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.json.simple.parser.ParseException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * Created by yejoon3117@mindslab.ai on 2020-11-09
 */
@Slf4j
@Controller
@RequiredArgsConstructor
@RequestMapping("/api/dmng")
public class DataRestController {
    private final DataRestService dataRestService;

    @ApiOperation(value = "학습 가능한 엔진 목록 조회")
    @GetMapping("/getEngineList")
    @PreAuthorize("hasAnyRole('USER')")
    public ResponseEntity<List<CodeVo>> getEngineList() throws Exception{

        String logTitle = "getEngineList/";

        try {
            List<CodeVo> result = dataRestService.getEngineList();
            log.info(logTitle + "success");
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (RuntimeException | ParseException e) {
            log.error(logTitle + "failed:" + e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ApiOperation(value = "언어 목록 조회")
    @GetMapping("/getLang")
    @PreAuthorize("hasAnyRole('USER')")
    public ResponseEntity<List<CodeVo>> getLang() throws ParseException {

        String logTitle = "getLang/";

        try {
            List<CodeVo> result = dataRestService.getLang();
            log.info(logTitle + "success");
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (RuntimeException e) {
            log.error(logTitle + "failed:" + e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ApiOperation(value = "STT sample rate type 목록 조회")
    @GetMapping("/getSampleRateList")
    @PreAuthorize("hasAnyRole('USER')")
    public ResponseEntity<List<CodeVo>> getSampleRateList() throws ParseException {

        String logTitle = "getSampleRateList/";

        try {
            List<CodeVo> result = dataRestService.getSampleRateList();
            log.info(logTitle + "success");
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (RuntimeException e) {
            log.error(logTitle + "failed:" + e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ApiOperation(value = "사용 가능한 데이터 목록 조회")
    @GetMapping("/getLearningDataList")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "engnType", value = "엔진 타입", dataType = "String"),
            @ApiImplicitParam(name = "langCd", value = "엔진 타입", dataType = "String"),
            @ApiImplicitParam(name = "useEosYn", value = "EOS 사용여부", dataType = "String"),
            @ApiImplicitParam(name = "smplRate", value = "sample rate 타입", dataType = "String")
    })
    @PreAuthorize("hasAnyRole('USER')")
    public ResponseEntity<List<DataFileInfoVo>> getLeaningDataList(@CurrentUser UserPrincipal user,
                                                                   String engnType,
                                                                   String langCd,
                                                                   String useEosYn,
                                                                   String smplRate) throws ParseException {
        String userId = user.getEmail();
        String logTitle = "getLearningDataList/userId[" + userId + "] engnType[" + engnType + "] langCd[" + langCd + "] useEosYn[" + useEosYn + "] smplRate[" + smplRate + "]/";

        try {
            List<DataFileInfoVo> result = dataRestService.getLearningDataList(userId, engnType, langCd, useEosYn, smplRate);
            log.info(logTitle + "success");
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (RuntimeException e) {
            log.error(logTitle + "failed:" + e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ApiOperation(value = "잡음 데이터 목록 조회")
    @GetMapping("/getNoiseDataList")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "smplRate", value = "sample rate 타입", dataType = "String")
    })
    @PreAuthorize("hasAnyRole('USER')")
    public ResponseEntity<List<DataFileInfoVo>> getNoiseDataList(@CurrentUser UserPrincipal user,
                                                                 String smplRate) throws ParseException {
        String userId = user.getEmail();
        String logTitle = "getNoiseDataList/userId[" + userId + "] smplRate[" + smplRate + "]/";

        try {
            List<DataFileInfoVo> result = dataRestService.getNoiseDataList(userId, smplRate);
            log.info(logTitle + "success");
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (RuntimeException e) {
            log.error(logTitle + "failed:" + e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ApiOperation(value = "공통 학습 모델 조회")
    @GetMapping("/getCommonModelList")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "engnType", value = "엔진 타입", dataType = "String"),
            @ApiImplicitParam(name = "langCd", value = "언어 타입", dataType = "String"),
            @ApiImplicitParam(name = "smplRate", value = "sample rate 타입", dataType = "String"),
            @ApiImplicitParam(name = "useEosYn", value = "EOS 사용여부", dataType = "String")
    })
    @PreAuthorize("hasAnyRole('USER')")
    public ResponseEntity<List<ModelInfoVo>> getCommonModelList(String engnType,
                                                                String langCd,
                                                                String smplRate,
                                                                String useEosYn) {

        String logTitle = "getCommonModelList/engnType[" + engnType + "] lang[" + langCd + "] smplRate[" + smplRate + "] useEosYn[" + useEosYn + "]/";

        try {
            List<ModelInfoVo> result = dataRestService.getCommonModelList(engnType, langCd, smplRate, useEosYn);
            log.info(logTitle + "success");
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (RuntimeException | ParseException e) {
            log.error(logTitle + "failed:" + e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ApiOperation(value = "사용자의 private 모델 조회")
    @GetMapping("/getMyModelList")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "engnType", value = "엔진 타입", dataType = "String"),
            @ApiImplicitParam(name = "langCd", value = "언어 타입", dataType = "String"),
            @ApiImplicitParam(name = "smplRate", value = "sample rate 타입", dataType = "String")
    })
    @PreAuthorize("hasAnyRole('USER')")
    public ResponseEntity<List<ModelInfoVo>> getMyModelList(@CurrentUser UserPrincipal user,
                                                            String engnType,
                                                            String langCd,
                                                            String smplRate,
                                                            String useEosYn) {
        String userId = user.getEmail();
        String logTitle = "getMyModelList/userId[" + userId +"] engnType[" + engnType + "] langCd[" + langCd + "] smplRate[" + smplRate + "] useEosYn[" + useEosYn + "]/";
        try {
            List<ModelInfoVo> result = dataRestService.getMyModelList(userId, engnType, langCd, smplRate, useEosYn);
            log.info(logTitle + "success");
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            log.error(logTitle + "failed:" + e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
    @ApiOperation(value = "학습 모델 ID로 모델 데이터 리스트 조회")
    @GetMapping("/getModelFileList/{projectId}")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "projectId", value = "프로젝트 ID", dataType = "String")
    })
    @PreAuthorize("hasAnyRole('USER')")
    public ResponseEntity<List<ModelInfoVo>> getModelFileList(@CurrentUser UserPrincipal user,
                                                              @PathVariable Long projectId) {
        String logTitle = "getMyModelList/projectId[" + projectId + "]/";

        try {
            List<ModelInfoVo> result = dataRestService.getModelFileList(user.getUserNo(), projectId);   // userNo는 프로젝트와 사용자 일치 여부 조회를 위해 사용
            log.info(logTitle + "success");
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            log.error(logTitle + "failed:" + e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
