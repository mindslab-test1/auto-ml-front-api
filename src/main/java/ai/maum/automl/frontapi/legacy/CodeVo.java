package ai.maum.automl.frontapi.legacy;

import lombok.*;

import java.io.Serializable;

/**
 * Created by yejoon3117@mindslab.ai on 2020-11-09
 */
@Getter
@Setter
@NoArgsConstructor
@ToString
public class CodeVo implements Serializable {
    String code;        // 공통코드
    String codeName;    // 코드명
    String codeDisp;    // 코드노출명
}
