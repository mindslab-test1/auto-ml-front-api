package ai.maum.automl.frontapi.repository;

import ai.maum.automl.frontapi.model.dto.project.ProjectSnapshotDto;
import ai.maum.automl.frontapi.model.dto.project.ProjectSnapshotDtoInterface;
import ai.maum.automl.frontapi.model.entity.project.ProjectSnapshot;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

/**
 * Created by kirk@mindslab.ai on 2020-10-22
 */
@Repository
public interface ProjectSnapshotRepository extends CrudRepository<ProjectSnapshot, Long> {
//    List<ProjectSnapshot> findAllByProjectIdOrderByRegDtDesc(Long id);
    @Query(value = "" +
            "select " +
            " ps.id, ps.name, ps.snapshot, ps.reg_dt as regDt, ps.model_id as modelId, ps.status, " +
            " gd.bestTer, gd.bestTerEpoch, gd.bestWer, gd.bestWerEpoch " +
            "from " +
            " project_snapshot ps " +
            " left outer join graph_data gd on ps.id = gd.project_snapshot_id " +
            "where ps.project_id = :id " +
            "order by ps.reg_dt desc" , nativeQuery = true)
    List<ProjectSnapshotDtoInterface> findAllByProjectIdOrderByRegDtDesc(Long id);
    Optional<ProjectSnapshot> findTop1ByProjectIdOrderByRegDtDesc(Long projectId);
    List<ProjectSnapshot.ModelId> findByProjectIdAndStatus(Long projectId, String status);
    @Transactional
    void deleteProjectSnapshotById(Long id);
}
