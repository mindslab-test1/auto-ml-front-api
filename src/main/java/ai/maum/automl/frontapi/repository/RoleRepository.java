package ai.maum.automl.frontapi.repository;

import ai.maum.automl.frontapi.model.entity.common.Role;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by kirk@mindslab.ai on 2020-10-05
 */
@Repository
public interface RoleRepository extends CrudRepository<Role, Long> {
    Role findByName(String name);
}
