package ai.maum.automl.frontapi.repository;

import ai.maum.automl.frontapi.model.entity.project.ModelInquiry;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Created by kirk@mindslab.ai on 2020-10-22
 */
@Repository
public interface ModelInquiryRepository extends CrudRepository<ModelInquiry, Long> {
    List<ModelInquiry> findByUserId(Long id);
    Optional<ModelInquiry> findByModelName(String modelName);
}
