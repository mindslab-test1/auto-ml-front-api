package ai.maum.automl.frontapi.repository;

import ai.maum.automl.frontapi.model.entity.project.GraphData;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by yejoon3117@mindslab.ai on 2020-11-24
 */
@Repository
public interface GraphDataRepository extends CrudRepository<GraphData, Long> {
    GraphData findByProjectSnapshotId(Long projectSnapshotId);
}
