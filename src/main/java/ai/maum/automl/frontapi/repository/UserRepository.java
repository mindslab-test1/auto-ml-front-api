package ai.maum.automl.frontapi.repository;

import ai.maum.automl.frontapi.model.entity.common.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Created by kirk@mindslab.ai on 2020-10-22
 */
@Repository
public interface UserRepository extends CrudRepository<User, Long> {
    Optional<User> findById(Long id);
    User findByEmail(String email);
    Optional<User> findAllById(Long id);
}
