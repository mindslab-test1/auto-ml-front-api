package ai.maum.automl.frontapi.repository;

import ai.maum.automl.frontapi.model.entity.project.GraphDataSnapshot;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by yejoon3117@mindslab.ai on 2020-11-24
 */
@Repository
public interface GraphDataSnapshotRepository extends CrudRepository<GraphDataSnapshot, Long> {
    List<GraphDataSnapshot.DataPoint> findByProjectSnapshotIdOrderByRegDtAsc(Long projectSnapshotId);
}
