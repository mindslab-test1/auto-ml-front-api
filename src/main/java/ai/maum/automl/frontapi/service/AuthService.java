package ai.maum.automl.frontapi.service;

import ai.maum.automl.frontapi.commons.security.JwtTokenProvider;
import ai.maum.automl.frontapi.model.TokenVO;
import ai.maum.automl.frontapi.model.entity.common.Role;
import ai.maum.automl.frontapi.model.entity.common.RoleName;
import ai.maum.automl.frontapi.model.entity.common.User;
import ai.maum.automl.frontapi.repository.RoleRepository;
import ai.maum.automl.frontapi.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service
public class AuthService {

    private final AuthenticationManager authManager;
    private final PasswordEncoder passwordEncoder;
    private final JwtTokenProvider jwtTokenProvider;
    private final SSOInterface ssoIf;
    private final UserRepository userRepo;
    private final RoleRepository roleRepo;

//    @Transactional
    public Authentication authenticate(String id, String password)
    {
        Authentication auth = authManager.authenticate(new UsernamePasswordAuthenticationToken(id, password));
        SecurityContextHolder.getContext().setAuthentication(auth);
        return auth;
    }

    /*
     ** 인증 서버로부터 AccessToken(유효시간 포함)과 RefreshToken(유효시간 포함)을 발급받는다.
     */
    public User getAuthToken(String code, String state, String requestUri) {

        TokenVO token = ssoIf.publishTokens(code, requestUri);
        if(token != null) {
            User user = userRepo.findByEmail(token.getEmail());
            if (user == null) {
                User newUser = new User();
                newUser.setEmail(token.getEmail());
                newUser.setName(token.getName());
                newUser.setPassword("1234");
                newUser.setApiId(token.getApiId());
                newUser.setApiKey(token.getApiKey());
                Role userRole = roleRepo.findByName(RoleName.ROLE_USER.name());

                newUser.setRole(userRole);

                newUser = userRepo.save(newUser);
                return newUser;
            } else {
                return user;
            }
        }
        else return null;
    }

    public String generateJwtToken(Authentication auth)
    {
        return jwtTokenProvider.generateToken(auth);
    }

    public String encode(String password)
    {
        return passwordEncoder.encode(password);
    }
}
