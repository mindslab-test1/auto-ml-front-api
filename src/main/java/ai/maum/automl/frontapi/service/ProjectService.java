package ai.maum.automl.frontapi.service;

import ai.maum.automl.frontapi.commons.utils.LocalDateTimeSerializer;
import ai.maum.automl.frontapi.commons.utils.ModelMapperService;
import ai.maum.automl.frontapi.model.dto.project.ProjectDto;
import ai.maum.automl.frontapi.model.dto.project.ProjectSnapshotDto;
import ai.maum.automl.frontapi.model.dto.project.ProjectSnapshotDtoInterface;
import ai.maum.automl.frontapi.model.entity.project.*;
import ai.maum.automl.frontapi.repository.ProjectRepository;
import ai.maum.automl.frontapi.repository.ProjectSnapshotRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

/**
 * Created by kirk@mindslab.ai on 2020-10-21
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class ProjectService {
    private final ModelMapperService modelMapperService;
    private final ProjectRepository projectRepository;
    private final ProjectSnapshotRepository projectSnapshotRepository;

    @Transactional
    public Page<ProjectDto.ProjectDtoEx> list(Long userNo, Pageable pageable) {

//        Page<Project> list = projectRepository.findAllByUserIdOrderByIdDesc(userNo, pageable);
//        return list.map(project -> modelMapperService.map(project, ProjectDto.class));

        Page<ProjectExInterface> list2 = projectRepository.findAllByUserId(userNo, pageable);
        Page<ProjectDto.ProjectDtoEx> p2 = list2.map(project -> modelMapperService.map(project, ProjectDto.ProjectDtoEx.class));
        return p2;
    }

    @Transactional
    public void delete(Long userNo, Long id) {
        String logTitle = "delete/userNo["+ userNo +"] id[" + id + "]/";
        Optional<Project> optional  = projectRepository.findById(id);
        if(optional.isPresent() && optional.get().getUser().getId().equals(userNo)) {
            log.info(logTitle + "success");
            projectRepository.deleteById(id);
        }
        else {
            log.error(logTitle + "failed: Empty result");
            throw new EmptyResultDataAccessException(0);
        }
    }

    @Transactional
    public ProjectSnapshotDto.Detail addSnapshot(Long userNo, Long id, String name) throws JsonProcessingException {
        String logTitle = "addSnapshot/id[" + id + "] + name[" + name + "]/";
        Optional<Project> optional  = projectRepository.findById(id);

        if(optional.isPresent() && optional.get().getUser().getId().equals(userNo)) {
            Project project = optional.get();

            String dtoString = "";
            ObjectMapper objectMapper = new ObjectMapper();
            SimpleModule simpleModule = new SimpleModule();
            simpleModule.addSerializer(LocalDateTime.class, new LocalDateTimeSerializer());
            objectMapper.registerModule(simpleModule);

            if("STT".equalsIgnoreCase(project.getProjectType())){
                ProjectStt projectStt = (ProjectStt)project;
                ProjectDto.Stt dto = modelMapperService.map(projectStt, ProjectDto.Stt.class);
                dtoString = objectMapper.writeValueAsString(dto);
            }
            else if("TTS".equalsIgnoreCase(project.getProjectType())) {
                ProjectTts projectTts = (ProjectTts)project;
                ProjectDto.Tts dto = modelMapperService.map(projectTts, ProjectDto.Tts.class);
                dtoString = objectMapper.writeValueAsString(dto);
            }
            else {
                return null;
            }
            log.info(logTitle + dtoString);

            ProjectSnapshot snapshot = new ProjectSnapshot();
            snapshot.setName(name);
            snapshot.setProjectId(project.getId());
            snapshot.setSnapshot(dtoString);
            snapshot = projectSnapshotRepository.save(snapshot);
            // snapshot model id insert
            snapshot.setModelId(project.getModelName() + "_" + snapshot.getId());

            return modelMapperService.map(snapshot, ProjectSnapshotDto.Detail.class);
        }
        else {
            log.error(logTitle + "failed: Empty result");
            throw new EmptyResultDataAccessException(0);
        }
    }

    @Transactional
    public List<ProjectSnapshotDto.Detail> listSnapshot(Long userNo, Long id) {
        String logTitle = "listSnapshot/id[" + id + "]/";
        Optional<Project> optional  = projectRepository.findById(id);
        if(optional.isPresent() && optional.get().getUser().getId().equals(userNo)) {
            List<ProjectSnapshotDtoInterface> list = projectSnapshotRepository.findAllByProjectIdOrderByRegDtDesc(id);
            log.info(logTitle + "success");
            return modelMapperService.mapAll(list, ProjectSnapshotDto.Detail.class);
        }
        else {
            log.error(logTitle + "failed : Empty result");
            throw new EmptyResultDataAccessException(0);
        }
    }

    @Transactional
    public void restoreSnapshot(Long userNo, Long projectId, Long snapshotId) throws JsonProcessingException {
        String logTitle = "restoreSnapshot/id[" + projectId + "], snapshotId[" + snapshotId + "]/";
        Optional<Project> optional  = projectRepository.findById(projectId);
        if(optional.isPresent() && optional.get().getUser().getId().equals(userNo)) {
            Project project = optional.get();

            Optional<ProjectSnapshot> op = projectSnapshotRepository.findById(snapshotId);
            if(op.isPresent()) {
                ProjectSnapshot projectSnapshot = op.get();
                ObjectMapper objectMapper = new ObjectMapper();

                if("STT".equalsIgnoreCase(project.getProjectType())){
                    ProjectDto.Stt dto = objectMapper.readValue(projectSnapshot.getSnapshot(), ProjectDto.Stt.class);

                    ProjectStt ps = modelMapperService.map(dto, ProjectStt.class);

                    ProjectStt projectStt = (ProjectStt)project;

                    // project member copy
                    projectStt.setProjectName(ps.getProjectName());
                    projectStt.setProjectDescription(ps.getProjectDescription());
                    projectStt.setProjectType(ps.getProjectType());
                    projectStt.setModelName(ps.getModelName());

                    // projectStt member copy
                    projectStt.setCompletedStep(ps.getCompletedStep());
                    projectStt.setLanguage(ps.getLanguage());
                    projectStt.setSampleRate(ps.getSampleRate());
                    projectStt.setEos(ps.getEos());
                    projectStt.setLearningDataList(ps.getLearningDataList());
                    projectStt.setEpochSize(ps.getEpochSize());
                    projectStt.setBatchSize(ps.getBatchSize());
                    projectStt.setRate(ps.getRate());
                    projectStt.setSampleLength(ps.getSampleLength());
                    projectStt.setMinNoiseSample(ps.getMinNoiseSample());
                    projectStt.setMaxNoiseSample(ps.getMaxNoiseSample());
                    projectStt.setMinSnr(ps.getMinSnr());
                    projectStt.setMaxSnr(ps.getMaxSnr());
                    projectStt.setNoiseDataList(ps.getNoiseDataList());
                    projectStt.setPreTrainedModel(ps.getPreTrainedModel());

                    projectRepository.save(projectStt);
                    log.info(logTitle + "success - STT");
                    return;
                }
                else if("TTS".equalsIgnoreCase(project.getProjectType())){
                    ProjectDto.Tts dto = objectMapper.readValue(projectSnapshot.getSnapshot(), ProjectDto.Tts.class);
                    project = modelMapperService.map(dto, ProjectTts.class);
                    projectRepository.save(project);
                    log.info(logTitle + "success - TTS");
                    return;
                }
            }
        }
        log.error(logTitle + "failed : Empty result");
        throw new EmptyResultDataAccessException(0);
    }

    public ProjectDto get(Long userNo, Long id) {
        String logTitle = "get/userNo[" + userNo + "] id[" + id + "]/";
        Optional<Project> optional  = projectRepository.findById(id);
        if(optional.isPresent() && optional.get().getUser().getId().equals(userNo)) {
            log.info(logTitle + "success");
            return modelMapperService.map(optional.get(), ProjectDto.class);
        }
        else {
            log.error(logTitle + "failed : Empty result");
            throw new EmptyResultDataAccessException(0);
        }
    }

}
