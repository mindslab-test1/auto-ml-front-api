package ai.maum.automl.frontapi.service;

import ai.maum.automl.frontapi.commons.utils.ModelMapperService;
import ai.maum.automl.frontapi.commons.utils.UUIDCreator;
import ai.maum.automl.frontapi.model.dto.common.UserDto;
import ai.maum.automl.frontapi.model.entity.common.Role;
import ai.maum.automl.frontapi.model.entity.common.RoleName;
import ai.maum.automl.frontapi.model.entity.common.User;
import ai.maum.automl.frontapi.repository.RoleRepository;
import ai.maum.automl.frontapi.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Slf4j
@RequiredArgsConstructor
@Service
public class UserService {

    private final UserRepository repo;
    private final AuthService authSvc;
    private final RoleRepository roleRepo;
    private final ModelMapperService modelMapperService;

    public User signup(User user, Role userRoles)
    {
        user.setRole(userRoles);
        user.setPassword(authSvc.encode(user.getPassword()));

        add(user);

        return user;
    }

    public UserDto.Base loadProfile(Long userId)
    {
        User entity = loadByUserNo(userId);
        if(entity == null) return null;

        return modelMapperService.map(entity, UserDto.Base.class);
    }

    public UserDto.Base updateProfile(UserDto.ReqUpdate req)
    {
        User entity = loadByUserNo(req.getId());

        entity.setName(req.getName());

        return modelMapperService.map(entity, UserDto.Base.class);
    }

    @Transactional
    public User add(User user) {
        return repo.save(user);
    }

    public User loadByUserNo(Long userNo)
    {
        return repo.findById(userNo).orElse(null);
    }

//    public User loadByUuid(String userUuid)
//    {
//        return repo.findByUuid(userUuid).orElse(null);
//    }

    public boolean doesUserExist(String email)
    {
        return loadByEmail(email) != null;
    }

    public User loadByEmail(String email)
    {
        return repo.findByEmail(email);
    }

    public Role getRole(RoleName roleName) {
        return roleRepo.findByName(roleName.name());
    }
}