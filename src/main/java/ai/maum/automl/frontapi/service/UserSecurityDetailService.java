package ai.maum.automl.frontapi.service;

import ai.maum.automl.frontapi.commons.security.UserPrincipal;
import ai.maum.automl.frontapi.config.CacheKey;
import ai.maum.automl.frontapi.repository.UserRepository;
import ai.maum.automl.frontapi.model.entity.common.User;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Slf4j
@RequiredArgsConstructor
@Service
public class UserSecurityDetailService implements UserDetailsService {

    private final UserRepository userRepo;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException
    {
        User user = userRepo.findByEmail(email);

        return UserPrincipal.create(user);
    }

    @Cacheable(value = CacheKey.loadUserById, key="#id", unless = "#result == null")
    public UserDetails loadUserById(Long id)
    {
        Optional<User> optUser = userRepo.findById(id);

        if(optUser.isEmpty()) {
            return null;
        }

        User user = optUser.get();
        return UserPrincipal.create(user);
    }
}
