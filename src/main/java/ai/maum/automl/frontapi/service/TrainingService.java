package ai.maum.automl.frontapi.service;

import ai.maum.automl.frontapi.model.entity.project.Project;
import ai.maum.automl.frontapi.model.entity.project.ProjectSnapshot;
import ai.maum.automl.frontapi.model.entity.project.ProjectStt;
import ai.maum.automl.frontapi.repository.ProjectRepository;
import ai.maum.automl.frontapi.repository.ProjectSnapshotRepository;
import com.google.gson.Gson;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor
public class TrainingService {

    @Value("${backend.api.url}")
    private String backendApiUrl;

    private final String trainApiPath = "/api/training";

    private final ProjectSnapshotRepository projectSnapshotRepository;
    private final ProjectRepository projectRepository;

    /**
     * snapshot이 로그인한 사용자의 것인지 조회
     **/
    private Optional<Project> getOptionalProject(Long snapshotId){
        Optional<ProjectSnapshot> optionalProjectSnapshot = projectSnapshotRepository.findById(snapshotId);
        Long projectId = optionalProjectSnapshot.get().getProjectId();
        Optional<Project> optionalProject = projectRepository.findById(projectId);

        return optionalProject;
    }

    /**
     * API method : POST
     * DESC : 학습 시작
    **/
    public Map<String, Object> beginTraining(Long userNo, Long snapshotId, String trainingId) throws ParseException {
        String logTitle = "beginTraining/snapshotId[" + snapshotId + "], trainingId[" + trainingId + "]/";
        Optional<Project> optionalProject = getOptionalProject(snapshotId);

        // snapshot이 로그인한 사용자의 것인지 조회
        if(optionalProject.isPresent() && optionalProject.get().getUser().getId().equals(userNo)) {
            String apiUrl = backendApiUrl + trainApiPath + "/begin";

            JSONObject reqObj = new JSONObject();
            reqObj.put("snapshotId", snapshotId);
            reqObj.put("trainingId", trainingId);

            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(new MediaType("application", "json", StandardCharsets.UTF_8));
            HttpEntity<String> entity = new HttpEntity<>(reqObj.toString(), headers);

            ResponseEntity<String> response = restTemplate.exchange(apiUrl, HttpMethod.POST, entity, String.class);

            int responseCode = response.getStatusCode().value();
            if(responseCode != 200) {
                log.error(logTitle + "failed : responseCode=" + responseCode);
                throw new RuntimeException();
            }

            JSONParser jsonParser = new JSONParser();
            JSONObject jsonObject = (JSONObject) jsonParser.parse(response.getBody());
            Map<String, Object> responseBody = new Gson().fromJson(jsonObject.toString(), HashMap.class);
            log.info(logTitle + "success");

            // 학습 요청 후 completedStep 업데이트
            ProjectStt project = (ProjectStt)optionalProject.get();
            if(project.getCompletedStep() < 2){
                project.setCompletedStep(2);
                projectRepository.save(project);
                log.info(logTitle + "update project step to 2");
            }

            if("false".equalsIgnoreCase(jsonObject.get("success").toString())) {    // api error
                responseBody.put("success", jsonObject.get("success"));
                responseBody.put("message", jsonObject.get("message"));
                responseBody.put("errorCode", jsonObject.get("errorCode"));
            } else {
                responseBody.put("success", jsonObject.get("success"));
                responseBody.put("message", jsonObject.get("message"));
            }

            return responseBody;
        }else{
            log.error(logTitle + "failed : Empty result");
            throw new EmptyResultDataAccessException(0);
        }


    }

    /**
     * API method : PUT
     * DESC : 학습 종료(학습중인 경우)
     **/
    public Map<String, Object> finishTraining(Long userNo, Long snapshotId, String trainingId) throws ParseException {
        String logTitle = "finishTraining/snapshotId[" + snapshotId + "], trainingId[" + trainingId + "]/";
        Optional<Project> optionalProject = getOptionalProject(snapshotId);

        // snapshot이 로그인한 사용자의 것인지 조회
        if(optionalProject.isPresent() && optionalProject.get().getUser().getId().equals(userNo)) {
            String apiUrl = backendApiUrl + trainApiPath + "/finish";

            JSONObject reqObj = new JSONObject();
            reqObj.put("snapshotId", snapshotId);
            reqObj.put("trainingId", trainingId);

            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(new MediaType("application", "json", StandardCharsets.UTF_8));
            HttpEntity<String> entity = new HttpEntity<>(reqObj.toString(), headers);

            ResponseEntity<String> response = restTemplate.exchange(apiUrl, HttpMethod.PUT, entity, String.class);

            int responseCode = response.getStatusCode().value();
            if(responseCode != 200) {
                log.error(logTitle + "failed : responseCode=" + responseCode);
                throw new RuntimeException();
            }

            JSONParser jsonParser = new JSONParser();
            JSONObject jsonObject = (JSONObject) jsonParser.parse(response.getBody());
            Map<String, Object> responseBody = new Gson().fromJson(jsonObject.toString(), HashMap.class);
            log.info(logTitle + "success");


            Optional<ProjectSnapshot> optionalProjectSnapshot = projectSnapshotRepository.findById(snapshotId);
            String status = optionalProjectSnapshot.get().getStatus();

            if("false".equalsIgnoreCase(jsonObject.get("success").toString())) {    // api error
                responseBody.put("success", jsonObject.get("success"));
                responseBody.put("message", jsonObject.get("message"));
                responseBody.put("errorCode", jsonObject.get("errorCode"));
            }
            else if(status.equals("e")) {
                responseBody.put("success", "fail");
                responseBody.put("message", "Training status : e");
                responseBody.put("errorCode", "");
            }
            else {
                // 학습 종료 후 completedStep 업데이트
                ProjectStt project = (ProjectStt)optionalProject.get();
                if(project.getCompletedStep() < 3){
                    project.setCompletedStep(3);
                    projectRepository.save(project);
                    log.info(logTitle + "update project step to 3");
                }

                responseBody.put("success", jsonObject.get("success"));
                responseBody.put("message", jsonObject.get("message"));
            }

            return responseBody;
        }else{
            log.error(logTitle + "failed : Empty result");
            throw new EmptyResultDataAccessException(0);
        }
    }


    /**
     * API method : PUT
     * DESC : 학습 취소(대기중인 경우)
     **/
    public Map<String, Object> cancelTraining(Long userNo, Long snapshotId, String trainingId) throws ParseException {
        String logTitle = "cancelTraining/snapshotId[" + snapshotId + "], trainingId[" + trainingId + "]/";
        Optional<Project> optionalProject = getOptionalProject(snapshotId);

        // snapshot이 로그인한 사용자의 것인지 조회
        if(optionalProject.isPresent() && optionalProject.get().getUser().getId().equals(userNo)) {
            String apiUrl = backendApiUrl + trainApiPath + "/cancel";

            JSONObject reqObj = new JSONObject();
            reqObj.put("snapshotId", snapshotId);
            reqObj.put("trainingId", trainingId);

            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(new MediaType("application", "json", StandardCharsets.UTF_8));
            HttpEntity<String> entity = new HttpEntity<>(reqObj.toString(), headers);

            ResponseEntity<String> response = restTemplate.exchange(apiUrl, HttpMethod.PUT, entity, String.class);

            int responseCode = response.getStatusCode().value();
            if(responseCode != 200) {
                log.error(logTitle + "failed : responseCode=" + responseCode);
                throw new RuntimeException();
            }

            JSONParser jsonParser = new JSONParser();
            JSONObject jsonObject = (JSONObject) jsonParser.parse(response.getBody());
            Map<String, Object> responseBody = new Gson().fromJson(jsonObject.toString(), HashMap.class);
            log.info(logTitle + "success");

            // snapshot delete
            projectSnapshotRepository.deleteProjectSnapshotById(snapshotId);

            // 학습 요청 후 completedStep 업데이트
            ProjectStt project = (ProjectStt)optionalProject.get();
            if(project.getCompletedStep() == 2){
                project.setCompletedStep(1);
                projectRepository.save(project);
                log.info(logTitle + "update project step to 1");
            }

            if("false".equalsIgnoreCase(jsonObject.get("success").toString())) {    // api error
                responseBody.put("success", jsonObject.get("success"));
                responseBody.put("message", jsonObject.get("message"));
                responseBody.put("errorCode", jsonObject.get("errorCode"));
            } else {
                responseBody.put("success", jsonObject.get("success"));
                responseBody.put("message", jsonObject.get("message"));
            }

            return responseBody;
        }else{
            log.error(logTitle + "failed : Empty result");
            throw new EmptyResultDataAccessException(0);
        }
    }
}
