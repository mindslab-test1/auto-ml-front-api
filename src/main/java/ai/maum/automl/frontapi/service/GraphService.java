package ai.maum.automl.frontapi.service;

import ai.maum.automl.frontapi.model.dto.project.GraphDto;
import ai.maum.automl.frontapi.model.entity.project.GraphData;
import ai.maum.automl.frontapi.model.entity.project.GraphDataSnapshot;
import ai.maum.automl.frontapi.model.entity.project.Project;
import ai.maum.automl.frontapi.model.entity.project.ProjectSnapshot;
import ai.maum.automl.frontapi.repository.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor
public class GraphService {

    private final ProjectRepository projectRepository;
    private final ProjectSnapshotRepository projectSnapshotRepository;
    private final QueueRepository queueRepository;
    private final GraphDataSnapshotRepository graphDataSnapshotRepository;
    private final GraphDataRepository graphDataRepository;

    public GraphDto monitoring(Long userNo, Long projectId) throws EmptyResultDataAccessException {
        String logTitle = "monitoring/userNo[" + userNo + "] projectId[" + projectId + "]/";

        Optional<Project> optionalProject = projectRepository.findById(projectId);

        // 프로젝트가 로그인한 사용자의 것인지 조회
        if(optionalProject.isPresent() && optionalProject.get().getUser().getId().equals(userNo)) {

            Optional<ProjectSnapshot> optionalProjectSnapshot  = projectSnapshotRepository.findTop1ByProjectIdOrderByRegDtDesc(projectId);

            if(optionalProjectSnapshot.isPresent()) {
                String status = optionalProjectSnapshot.get().getStatus();

                if("q".equalsIgnoreCase(status)) {          // 학습 대기 중

                    return queueInfo(optionalProjectSnapshot.get().getId());

                } else if("t".equalsIgnoreCase(status)) {   // 학습 진행 중

                    return ongoingGraph(optionalProjectSnapshot.get().getId());

                } else if("c".equalsIgnoreCase(status)) {   // 학습 완료

                    Long projectSnapshotId = optionalProjectSnapshot.get().getId();
                    return completedGraph(projectSnapshotId);

                } else if("e".equalsIgnoreCase(status)) {   // 학습 중 에러
                    return errorGraph();
                } else {
                    log.error(logTitle + "failed : unknown status ");
                }
            }
            return null;
        }
        else {
            log.error(logTitle + "failed : Empty result");
            throw new EmptyResultDataAccessException(0);
        }
    }

    /* queue table 조회하여 순번 알려주기 */
    public GraphDto queueInfo(Long projectSnapshotId) {
        GraphDto graphDto = new GraphDto();

        int order = queueRepository.findOrder(projectSnapshotId);
        Optional<ProjectSnapshot> entity = projectSnapshotRepository.findById(projectSnapshotId);

        if(entity.isPresent()) {
            graphDto.setModelId(entity.get().getModelId());
        } else {
            graphDto.setModelId("");
        }

        graphDto.setStatus("q");
        graphDto.setSnapshotId(projectSnapshotId);
        graphDto.setOrder(order);

        return graphDto;
    }

    /* graph_data_snapshot 값들 조회 후 조합된 값 알려주기 */
    public GraphDto ongoingGraph(Long projectSnapshotId) {
        GraphDto graphDto = new GraphDto();
        JSONArray jsonArray = new JSONArray();

        List<GraphDataSnapshot.DataPoint> graphDataSnapshotList = graphDataSnapshotRepository.findByProjectSnapshotIdOrderByRegDtAsc(projectSnapshotId);

        if (graphDataSnapshotList == null) {
            throw new EmptyResultDataAccessException(0);
        }

        graphDataSnapshotList.forEach(dataPoint -> {
            String str = dataPoint.getDataPoints();
            try {
                JSONParser jsonParser = new JSONParser();
                JSONObject jsonObject = (JSONObject) jsonParser.parse(str);
                jsonArray.add(jsonObject);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        });

        Optional<ProjectSnapshot> entity = projectSnapshotRepository.findById(projectSnapshotId);

        if(entity.isPresent()) {
            graphDto.setModelId(entity.get().getModelId());
        } else {
            graphDto.setModelId("");
        }

        graphDto.setStatus("t");
        graphDto.setSnapshotId(projectSnapshotId);
        graphDto.setDataPoints(jsonArray);

        return graphDto;
    }

    /* graph_data 값 조회하여 알려주기 */
    public GraphDto completedGraph(Long projectSnapshotId) {
        GraphDto graphDto = new GraphDto();
        JSONArray jsonArray = new JSONArray();

        GraphData graphData = graphDataRepository.findByProjectSnapshotId(projectSnapshotId);

        if(graphData == null) {
            throw new EmptyResultDataAccessException(0);
        }

        String str = graphData.getDataPoints();
        try {
            JSONParser jsonParser = new JSONParser();
            jsonArray = (JSONArray) jsonParser.parse(str);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Optional<ProjectSnapshot> entity = projectSnapshotRepository.findById(projectSnapshotId);

        if(entity.isPresent()) {
            graphDto.setModelId(entity.get().getModelId());
        } else {
            graphDto.setModelId("");
        }

        graphDto.setStatus("c");
        graphDto.setSnapshotId(graphData.getProjectSnapshotId());
        graphDto.setDataPoints(jsonArray);

        return graphDto;
    }

    public GraphDto errorGraph() {
        GraphDto graphDto = new GraphDto();
        graphDto.setStatus("e");
        return graphDto;
    }
}
