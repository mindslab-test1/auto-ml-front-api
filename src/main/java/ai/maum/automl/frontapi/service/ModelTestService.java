package ai.maum.automl.frontapi.service;

import ai.maum.automl.frontapi.commons.utils.ByteArrayResourceWithFileName;
import com.google.gson.Gson;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.HashMap;

@Slf4j
@Service
@RequiredArgsConstructor
public class ModelTestService {

    @Value("${backend.api.url}")
    private String backendApiUrl;

    private final String testApiPath = "/api/test";

    /**
     * Desc : 모델 테스트 생성 요청
     * */
    public HashMap<String, Object> beginModelTest(String modelId, String modelFileName, String modelFilePath, MultipartFile dataFile) throws ParseException, IOException {
        String logTitle = "beginModelTest/modelId[" + modelId + "] modelFileName[" + modelFileName + "] modelFilePath[" + modelFilePath + "] dataFile[" + dataFile + "]/";
        String apiUrl = backendApiUrl + testApiPath + "/begin";

        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        headers.setAccept(Arrays.asList(MediaType.ALL));

        MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
        body.add("modelId", modelId);
        body.add("modelFilePath", modelFilePath);
        body.add("modelFileName", modelFileName);
        body.add("file", dataFile.getResource());
        //body.add("file", new ByteArrayResourceWithFileName(dataFile.getOriginalFilename(), dataFile.getBytes()));

        HttpEntity<MultiValueMap<String, Object>> reqEntity = new HttpEntity<>(body, headers);
        ResponseEntity<String> response = restTemplate.postForEntity(apiUrl, reqEntity, String.class);

        int responseCode = response.getStatusCode().value();
        if (responseCode != 200) {
            System.out.println(response.toString());
            log.error(logTitle + "failed : responseCode=" + responseCode);
            throw new RuntimeException();
        }

        JSONParser jsonParser = new JSONParser();

        JSONObject jsonObject = (JSONObject) jsonParser.parse(response.getBody());
        //HashMap<String, Object> responseBody = new Gson().fromJson(jsonObject.toString(), HashMap.class);
        HashMap<String, Object> responseBody = new HashMap<>();

        if("false".equalsIgnoreCase(jsonObject.get("success").toString())) {    // api error
            responseBody.put("success", jsonObject.get("success"));
            responseBody.put("message", jsonObject.get("message"));
            responseBody.put("errorCode", jsonObject.get("errorCode"));
        } else {
            responseBody.put("success", jsonObject.get("success"));
            responseBody.put("status", jsonObject.get("status"));
            responseBody.put("modelTestSnapshotId", jsonObject.get("modelTestSnapshotId"));
            responseBody.put("queueIndex", jsonObject.get("queueIndex"));
        }

        log.info(logTitle + "success");

        return responseBody;
    }

    /**
     * DESC : 대기열에 있거나 실행중인 모델 테스트의 상태값 조회
     */
    public HashMap<String, Object> getModelTestStatus(Long userNo, Long modelTestSnapshotId) throws ParseException {
        String logTitle = "getModelTestStatus/modelTestSnapshotId[" + modelTestSnapshotId + "]/";
        String apiUrl = backendApiUrl + testApiPath + "/status";

        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(new MediaType("application", "json", Charset.forName("UTF-8")));

        UriComponents builder = UriComponentsBuilder.fromHttpUrl(apiUrl)
                .queryParam("modelTestSnapshotId", modelTestSnapshotId)
                .build(false);
        ResponseEntity<String> response = restTemplate.exchange(builder.toUriString(), HttpMethod.GET, new HttpEntity<String>(headers), String.class);

        int responseCode = response.getStatusCode().value();
        if (responseCode != 200) {
            log.error(logTitle + "failed : responseCode=" + responseCode);
            throw new RuntimeException();
        }

        JSONParser jsonParser = new JSONParser();
        JSONObject jsonObject = (JSONObject) jsonParser.parse(response.getBody());
        //HashMap<String, Object> responseBody = new Gson().fromJson(jsonObject.toString(), HashMap.class);
        HashMap<String, Object> responseBody = new HashMap<>();

        if("false".equalsIgnoreCase(jsonObject.get("success").toString())) {    // api error
            responseBody.put("success", jsonObject.get("success"));
            responseBody.put("message", jsonObject.get("message"));
            responseBody.put("errorCode", jsonObject.get("errorCode"));
        } else {
            responseBody.put("success", jsonObject.get("success"));
            responseBody.put("status", jsonObject.get("status"));
            responseBody.put("result", jsonObject.get("result"));
            responseBody.put("modelTestSnapshotId", jsonObject.get("modelTestSnapshotId"));
            responseBody.put("queueIndex", jsonObject.get("queueIndex"));
        }

        log.info(logTitle + "success");

        return responseBody;
    }

    /**
     * DESC : 대기열에 있는 모델 테스트 취소
     */
    public HashMap<String, Object> cancelModelTest(Long userNo, Long modelTestSnapshotId) throws ParseException {
        String logTitle = "cancelModelTest/modelTestSnapshotId[" + modelTestSnapshotId + "]/";
        String apiUrl = backendApiUrl + testApiPath + "/cancel";

        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(new MediaType("application", "json", Charset.forName("UTF-8")));

        UriComponents builder = UriComponentsBuilder.fromHttpUrl(apiUrl)
                .queryParam("modelTestSnapshotId", modelTestSnapshotId)
                .build(false);
        ResponseEntity<String> response = restTemplate.exchange(builder.toUriString(), HttpMethod.PUT, new HttpEntity<String>(headers), String.class);

        int responseCode = response.getStatusCode().value();
        if (responseCode != 200) {
            log.error(logTitle + "failed : responseCode=" + responseCode);
            throw new RuntimeException();
        }

        JSONParser jsonParser = new JSONParser();
        JSONObject jsonObject = (JSONObject) jsonParser.parse(response.getBody());
        //HashMap<String, Object> responseBody = new Gson().fromJson(jsonObject.toString(), HashMap.class);
        HashMap<String, Object> responseBody = new HashMap<>();

        if("false".equalsIgnoreCase(jsonObject.get("success").toString())) {    // api error
            responseBody.put("success", jsonObject.get("success"));
            responseBody.put("message", jsonObject.get("message"));
            responseBody.put("errorCode", jsonObject.get("errorCode"));
        } else {
            responseBody.put("success", jsonObject.get("success"));
            responseBody.put("message", jsonObject.get("message"));
        }

        log.info(logTitle + "success");

        return responseBody;
    }
}
