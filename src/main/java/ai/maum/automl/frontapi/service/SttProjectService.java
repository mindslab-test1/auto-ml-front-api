package ai.maum.automl.frontapi.service;

import ai.maum.automl.frontapi.commons.utils.ModelMapperService;
import ai.maum.automl.frontapi.model.dto.project.ProjectDto;
import ai.maum.automl.frontapi.model.entity.common.User;
import ai.maum.automl.frontapi.model.entity.project.Project;
import ai.maum.automl.frontapi.model.entity.project.ProjectStt;
import ai.maum.automl.frontapi.repository.ProjectRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

/**
 * Created by kirk@mindslab.ai on 2020-10-20
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class SttProjectService {

    private final ModelMapperService modelMapperService;

    private final ProjectRepository projectRepository;

    @Transactional
    public ProjectDto.Stt add(Long userNo, ProjectDto dto) {
        if("STT".equalsIgnoreCase(dto.getProjectType())) {
            User user = new User(userNo);
            ProjectStt projectStt = modelMapperService.map(dto, ProjectStt.class);
            projectStt.setCompletedStep(0);
            projectStt.setUser(user);

            String modelName = userNo.toString() + "_STT_" + LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmssSSS"));

            projectStt.setModelName(modelName);
            projectStt = projectRepository.save(projectStt);

            return modelMapperService.map(projectStt, ProjectDto.Stt.class);
        }
        else {
            log.error("not supported projectType:" + dto.getProjectType());
            return null;
        }
    }

    @Transactional
    public ProjectDto.Stt get(Long userNo, Long id) {
        Optional<Project> optional  = projectRepository.findById(id);
        if(optional.isPresent() && optional.get().getUser().getId().equals(userNo)) {
            Project project = optional.get();
            if("STT".equalsIgnoreCase(project.getProjectType())){
                ProjectStt projectStt = (ProjectStt)project;
                ProjectDto.Stt dto = modelMapperService.map(projectStt, ProjectDto.Stt.class);
                return dto;
            }
            else {
                return null;
            }
        }
        else {
            throw new EmptyResultDataAccessException(0);
        }
    }
    
    @Transactional
    public void updateStep(Long userNo, Long id, int step){
        Optional<Project> optional  = projectRepository.findById(id);
        if(optional.isPresent() && optional.get().getUser().getId().equals(userNo)) {
            ProjectStt project = (ProjectStt)optional.get();
            project.setCompletedStep(step);
            projectRepository.save(project);
        } else {
            throw new EmptyResultDataAccessException(0);
        }
    }

    @Transactional
    public void learningData(Long userNo, Long id, ProjectDto.LearningDataInput dto) {
        Optional<Project> optional  = projectRepository.findById(id);
        if(optional.isPresent() && optional.get().getUser().getId().equals(userNo)) {
            ProjectStt project = (ProjectStt)optional.get();
            project.setLanguage(dto.getLanguage());
            project.setSampleRate(dto.getSampleRate());
            project.setEos(dto.getEos());
            project.setLearningDataList(dto.getLearningDataList());

            //TODO: step enum 만들것.
            if(project.getCompletedStep() < 1)
                project.setCompletedStep(1);

            projectRepository.save(project);
        } else {
            throw new EmptyResultDataAccessException(0);
        }
    }

    @Transactional
    public void trainingData(Long userNo, Long id, ProjectDto.TrainingDataInput dto) {
        Optional<Project> optional  = projectRepository.findById(id);
        if(optional.isPresent() && optional.get().getUser().getId().equals(userNo)) {
            ProjectStt project = (ProjectStt)optional.get();
            project.setEpochSize(dto.getEpochSize());
            project.setBatchSize(dto.getBatchSize());
            project.setRate(dto.getRate());
            project.setSampleLength(dto.getSampleLength());
            project.setNoiseDataList(dto.getNoiseDataList());
            project.setMinNoiseSample(dto.getMinNoiseSample());
            project.setMaxNoiseSample(dto.getMaxNoiseSample());
            project.setMinSnr(dto.getMinSnr());
            project.setMaxSnr(dto.getMaxSnr());
            project.setCheckSave(dto.getCheckSave());
            project.setPreTrainedModel(dto.getPreTrainedModel());
            //TODO: step enum 만들것.
//            if(project.getCompletedStep() < 2)
//                project.setCompletedStep(2);

            projectRepository.save(project);
        }
        else {
            throw new EmptyResultDataAccessException(0);
        }
    }
}
